'''
Asynchronus calls to the server
'''



var googirls = {}
googirls.word = '';

googirls.setAjax = function() {
  googirls.word = document.getElementById('search_input').value;
  googirls.sendAjax('/define?word=', googirls.word, googirls.definitionResponseHandler);
  googirls.sendAjax('/search?word=', googirls.word, googirls.resultResponseHandler);
};

googirls.sendAjax = function(url, word, handler){
  var request = new XMLHttpRequest();
  request.open('POST', url+word);
  request.onreadystatechange = handler;
  request.send();
};

document.getElementById('search_button').addEventListener('click', googirls.setAjax, false);

googirls.resultResponseHandler = function(){
  if(this.readyState == 4 && this.status == 200){
  	var result = document.createTextNode(this.responseText);
  	var result_box = document.getElementById('result_text');
    googirls.removeChildsFromDiv(result_box);
  	result_box.appendChild(result);
  }
  else if(this.readyState == 4 && this.status == 500){
    googirls.sendAjax('/search?word=', googirls.word, googirls.resultResponseHandler);
  }
};

googirls.definitionResponseHandler = function(){
  if(this.readyState == 4 && this.status == 200){
  	var definition = document.createTextNode(this.responseText);
  	var definition_box = document.getElementById('definition_text');
    googirls.removeChildsFromDiv(definition_box);
  	definition_box.appendChild(definition);
  }
  else if(this.readyState == 4 && this.status == 500){
    googirls.sendAjax('/define?word=', googirls.word, googirls.definitionResponseHandler);
  }
};

//TODO: parse JSON example:
/*
var sendAjax = function() {
  var req = new XMLHttpRequest();
  req.open('GET', 'data.json');
  req.onreadystatechange = myResponseHandler;
  req.send();
};
var myResponseHandler = function(){
  if (this.readyState == 4 && this.status == 200) {
    var jsonResponse =  JSON.parse(this.responseText); //important!
    alert(jsonResponse.apollo11[0]); //important!
  }
};
*/


googirls.removeChildsFromDiv = function(div){
  while (div.hasChildNodes()) {
      div.removeChild(div.firstChild);
    }
}

