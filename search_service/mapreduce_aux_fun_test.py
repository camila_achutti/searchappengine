
import unittest
from mapreduce_aux_fun import split_into_sentences
from mapreduce_aux_fun import split_into_words


class TestAuxFunctions(unittest.TestCase):

  def test_split_into_sentences(self):

  	self.assertEqual([''],split_into_sentences('')) # test empty string

  	text = 'To be. or not. to be, that is the question.'
  	expected_result = ['To be', 'or not', 'to be, that is the question', '']
  	self.assertEqual(expected_result, split_into_sentences(text))

  	text = 'To be. or not? to be, that is the question! '
  	self.assertEqual(expected_result, split_into_sentences(text))

  def test_split_into_words(self):

  	self.assertEqual([], split_into_words('')) # test empty string

  	sentence = 'To be or not to be, that is the question'
  	expected_result = ['To', 'be','or','not', 'to', 'be', 'that', 'is', 'the', 'question']
  	self.assertEqual(expected_result, split_into_words(sentence))

  def test_index_line_map(self):

  	self.assertEqual()


if __name__ == '__main__':
  unittest.main()