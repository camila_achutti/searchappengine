"""
  Auxiliary functions to be used by mapreduce
"""
import re
import logging

def split_into_sentences(text):
  """Split text into list of sentences."""
  text = re.sub(r"\s+", " ", text)
  text = re.sub(r"[\\.\\?\\!]\s?", "\n", text)
  return text.split("\n")

def split_into_words(sentence):
  """Split a sentence into list of words."""
  sentence = re.sub(r"\W+", " ", sentence)
  sentence = re.sub(r"[_0-9]+", " ", sentence)
  return sentence.split()

def index_line_map(data):
  """Word count map function."""
  print 'data looks like this:', data
  (entry, text_fn) = data
  text = text_fn()

  logging.debug("Got %s", entry.filename)
  for sentence in split_into_sentences(text):
    for word in split_into_words(sentence.lower()):
      yield (word, sentence)