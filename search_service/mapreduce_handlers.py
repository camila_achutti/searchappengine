from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import blobstore
from google.appengine.ext import db
from google.appengine.api import users
from mapreduce import base_handler
from mapreduce import mapreduce_pipeline
from mapreduce_models import FileMetadata
import urllib
import logging
import datetime

NUMBER_OF_SHARDS = 16

class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
  """Handler to upload data to blobstore."""

  def post(self):
    source = "uploaded by user"
    upload_files = self.get_uploads("file")
    blob_key = upload_files[0].key()
    name = self.request.get("name")

    user = users.get_current_user()

    username = user.nickname()
    date = datetime.datetime.now()
    str_blob_key = str(blob_key)
    key = FileMetadata.getKeyName(username, date, str_blob_key)

    file_metadata = FileMetadata(key_name = key)
    file_metadata.owner = user
    file_metadata.filename = name
    file_metadata.uploadedOn = date
    file_metadata.source = source
    file_metadata.blobkey = str_blob_key
    file_metadata.put()

    self.redirect("/")


class DownloadHandler(blobstore_handlers.BlobstoreDownloadHandler):
  """Handler to download blob by blobkey."""

  def get(self, key):
    key = str(urllib.unquote(key)).strip()
    logging.debug("key is %s" % key)
    blob_info = blobstore.BlobInfo.get(key)
    self.send_blob(blob_info)


class StoreOutput(base_handler.PipelineBase):
  """
  Args:
    encoded_key: the DB key corresponding to the metadata of this job
    output: the blobstore location where the output of the job is stored
  """

  def run(self, encoded_key, output):
    key = db.Key(encoded=encoded_key)
    m = FileMetadata.get(key)

    m.indexline_link = output[0]
    m.put()
    logging.debug("output is %s" % str(output))


class IndexLinePipeline(base_handler.PipelineBase):
  """
  Args:
    blobkey: blobkey to process as string. Should be a zip archive with
      text files inside.
  """

  def run(self, filekey, blobkey):
    logging.debug("filename is %s" % filekey)
    output = yield mapreduce_pipeline.MapreducePipeline(
        "index_line",
        "main.index_line_map",
        "main.index_line_reduce",
        "mapreduce.input_readers.BlobstoreZipInputReader",
        "mapreduce.output_writers.BlobstoreOutputWriter",
        mapper_params={
            "blob_key": blobkey,
        },
        reducer_params={
            "mime_type": "text/plain",
        },
        shards=NUMBER_OF_SHARDS)
    yield StoreOutput(filekey, output)

