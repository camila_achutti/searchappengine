from protorpc import messages
from protorpc import remote
from protorpc.wsgi import service
from mapreduce_models import IndexResult


'''
Implements an RPC search service

example of use:
curl -H \
   'content-type:application/json' \
   -d '{"word": "food"}'\
   http://localhost:9080/search_service.search
'''


class SearchRequest(messages.Message):
  '''Request string containing the word to be searched
  '''
  word = messages.StringField(1, required=True)


class SearchResponse(messages.Message):
  '''Response list of strings containing the result of the search
  '''
  result = messages.StringField( 1, repeated=True)


class SearchService(remote.Service):

  @remote.method(SearchRequest, SearchResponse)
  def search(self, request):
    '''searches the request.word in the index database and return the search
    result.
    '''
    word = request.word
    query_result = IndexResult.all().filter('word =', word).get()
    if query_result:
      search_result = query_result.values
    else:
      search_result = ['Word Not Found.']

    return SearchResponse(result=search_result)


app = service.service_mappings([('/search_service', SearchService)])
