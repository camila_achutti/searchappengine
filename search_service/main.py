#!/usr/bin/env python
#
# Copyright 2011 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import jinja2
import webapp2
from google.appengine.ext import blobstore
from google.appengine.api import users
from mapreduce_handlers import UploadHandler
from mapreduce_handlers import DownloadHandler
from mapreduce_handlers import IndexLinePipeline
from mapreduce_aux_fun import index_line_map
from mapreduce_models import IndexResult
from mapreduce_models import FileMetadata


NUMBER_OF_FILES_TO_SHOW = 10


class IndexHandler(webapp2.RequestHandler):
  """
  The main page that users will interact with, which presents users with
  the ability to upload new data or run MapReduce jobs on their existing data.
  """

  template_env = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"),
                                    autoescape=True)

  def get(self):
    user = users.get_current_user()
    username = user.nickname()

    first = FileMetadata.getFirstKeyForUser(username)
    last = FileMetadata.getLastKeyForUser(username)

    stored_files = FileMetadata.all()
    stored_files.filter("__key__ >", first)
    stored_files.filter("__key__ < ", last)
    results = stored_files.fetch(NUMBER_OF_FILES_TO_SHOW)

    items = [result for result in results]
    length = len(items)

    upload_url = blobstore.create_upload_url("/upload")

    self.response.out.write(self.template_env.get_template("index.html").render(
        {"username": username,
         "items": items,
         "length": length,
         "upload_url": upload_url}))

  def post(self):
    filekey = self.request.get("filekey")
    blob_key = self.request.get("blobkey")

    if self.request.get("index_line"):
      pipeline = IndexLinePipeline(filekey, blob_key)

    pipeline.start()
    self.redirect(pipeline.base_path + "/" + pipeline.pipeline_id)


def index_line_reduce(key, values):
  """Word count reduce function."""
  index_object = IndexResult(word = key, values = values)
  index_object.put()
  yield "%s: %s\n" % (key, values)


app = webapp2.WSGIApplication(
    [
        ('/', IndexHandler),
        ('/upload', UploadHandler),
        (r'/blobstore/(.*)', DownloadHandler),
    ],
    debug=True)


