"""This file contains the tests for the handlers of the application,
in search_handlers.py
"""

import mock
import unittest
import search_handlers


#TODO: implement test for the handlers using mock
class TestMainHandler(unittest.TestCase):
  """Test for MainHandler class
  """

  def test_get(self):
  	pass


class TestWordSearchHandler(unittest.TestCase):
  """Test for WordSearchHandler class
  """

  def test_post(self):
  	pass


class TestWordDefinitionHandler(unittest.TestCase):
  """Test for WordDefinitionHandler class
  """

  def test_post(self):
  	pass


class TestRPC(unittest.TestCase):

  def test_rpc():
  	"""Test for rpc function
  	"""
	pass


if __name__ == '__main__':
  unittest.main()