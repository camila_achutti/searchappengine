'''class containing the stub classes and methods (client side) for
Search and Definition services
'''

from protorpc import transport
from protorpc import messages
from protorpc import remote
package = 'message_handlers'

class DefinitionRequest(messages.Message):
  '''Request string containing the word to be defined
  '''

  term = messages.StringField(1, required=True)


class DefinitionResponse(messages.Message):
  '''Response string containing the result of the definition
  '''

  definition = messages.StringField(1, repeated=True)


class DefinitionService(remote.Service):

  @remote.method('DefinitionRequest', 'DefinitionResponse')
  def define(self, request):
    raise NotImplementedError('Method define is not implemented')


class SearchRequest(messages.Message):
  '''Request string containing the word to be searched
  '''
  word = messages.StringField(1, required=True)


class SearchResponse(messages.Message):
  '''Response list of strings containing the result of the search
  '''
  result = messages.StringField( 1, repeated=True)


class SearchService(remote.Service):

  @remote.method(SearchRequest, SearchResponse)
  def search(self, request):
  	raise NotImplementedError('Method search is not implemented')

