"""This file contains the service handlers of the application, that support 2 services: definition and search"""
import os
import urllib2
import cgi
import webapp2
import jinja2
from protorpc import transport
from protorpc import remote
from protorpc import protojson
import message_handlers

SEARCH_URL = 'http://localhost:9080/search_service'
DEFINITION_URL = 'http://definition-server.appspot.com/definition'
TEMPLATE_PATH = '/templates/shakespeare_index.html'
SPEECH_TEMPLATE_PATH = '/templates/speech_example.html'


class MainHandler(webapp2.RequestHandler):
  """Handles the request from the client and send them back the main app page.
  """

  def get(self):
    template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(
      os.getcwd()))
    template = template_env.get_template(TEMPLATE_PATH)
    self.response.out.write(template.render())


class WordSearchHandler(webapp2.RequestHandler):
  """Handles the request from the client for searching a word and responds with
  the result of the search (by calling to a search service).
  """

  def post(self):
    postservice = message_handlers.SearchService.Stub(
      transport.HttpTransport(SEARCH_URL))
    result = postservice.search(word=cgi.escape(self.request.get('word')))
    self.response.write(protojson.encode_message(result))


class WordDefinitionHandler(webapp2.RequestHandler):
  """Handles the request from the client for defining a word and responds with
  the result of the definition (by calling to a definition service).
  """

  def post(self):
    postservice = message_handlers.DefinitionService.Stub(
      transport.HttpTransport(DEFINITION_URL))
    result = postservice.define(term=cgi.escape(self.request.get('word')))
    if (not result.definition):
      result.definition.append('Word not found')
    self.response.write(protojson.encode_message(result))


class SpeechHandler(webapp2.RequestHandler):
  """Handles the example of speech recognition
  """

  def get(self):
    template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(
      os.getcwd()))
    template = template_env.get_template(SPEECH_TEMPLATE_PATH)
    self.response.out.write(template.render())


