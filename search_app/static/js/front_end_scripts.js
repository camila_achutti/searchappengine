""" related with the front-end scripts """

$(function(){
	var toggleResultContainer = function() {
		$(this).siblings().toggle();
	};

	$("h4").on("click", toggleResultContainer);
});

$(function() {
    var availableAuthors = [
      "All",
      "Shakespeare",
      "Isabel Allende",
      "Machado de Assis"
    ];
    $( '#authors' ).autocomplete({
      source: availableAuthors
    });
  });

$(function() {
  var availableBooks = [
    "All",
    "A Midsummer Night's Dream",
    "As You Like It",
    "Hamlet",
    "Macbeth",
    "King Lear",
    "Romeo and Juliet",
    "The House of the Spirits",
  "Of Love and Shadows", 
  "Eva Luna", 
  "The Stories of Eva Luna",
  "The Infinite Plan", 
    "Chrysalids",
    "Phalaenae",
    "Tales from Rio",
    "Resurrection",
    "Stories of Midnight", 
    "The Hand and the Glove"
  ];
  $( '#books' ).autocomplete({
    source: availableBooks
  });
});

