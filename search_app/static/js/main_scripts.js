/*
Asynchronus calls to the definition and index server
*/

var asyncCalls = {}
asyncCalls.ERROR = 500;
asyncCalls.OK = 200;
asyncCalls.READY = 4;
asyncCalls.ENTER = 13;
asyncCalls.word = '';


asyncCalls.setAjax = function() {
  asyncCalls.word = document.getElementById('search_input').value.toLowerCase();
  asyncCalls.sendAjax('/define?word=', asyncCalls.word, asyncCalls.definitionResponseHandler);
  asyncCalls.sendAjax('/search?word=', asyncCalls.word, asyncCalls.resultResponseHandler);
};


asyncCalls.sendAjax = function(url, word, handler){
  var request = new XMLHttpRequest();
  request.open('POST', url+word);
  request.onreadystatechange = handler;
  request.send();
};


asyncCalls.resultResponseHandler = function(){
  var result_box = document.getElementById('result_text');
  asyncCalls.removeChildsFromDiv(result_box);
  if(this.readyState == asyncCalls.READY && this.status == asyncCalls.OK){
    var results = {};
    if(this.responseText){
      results = JSON.parse(this.responseText);
      for(value in results.result){
        var paragraph = document.createElement('p');
        var result = document.createTextNode((Number(value) + 1) + ': '+ results.result[value]);
        paragraph.appendChild(result);
        result_box.appendChild(paragraph);
        if(speech){
          speech.say(results.result[value]);
        }
        if (results.result[value] != 'Word Not Found.'){
          script_js = document.createElement("script");
          script_js.type = "text/javascript";
          script_js.src = "//platform.twitter.com/widgets.js";
          var twitter = document.createElement('a');
          twitter.setAttribute('href', 'https://twitter.com/share');
          twitter.setAttribute('class', 'twitter-share-button');
          twitter.setAttribute('data-url','shakespeare_search.appspot.com');
          twitter.setAttribute('data-text', asyncCalls.word.toUpperCase() + ': ' + results.result[value]);
          twitter.setAttribute('data-via', 'shakespeare_search');
          twitter.innerHTML = "Tweet";
          var br = document.createElement('br');
          result_box.appendChild(script_js);
          result_box.appendChild(twitter);
          result_box.appendChild(br);
        }
      }
    }
  }else if(this.readyState == asyncCalls.READY && this.status == asyncCalls.ERROR){
    asyncCalls.sendAjax('/search?word=', asyncCalls.word, asyncCalls.resultResponseHandler);
  }
};



asyncCalls.definitionResponseHandler = function(){
  var definition_box = document.getElementById('definition_text');
  asyncCalls.removeChildsFromDiv(definition_box);
  if(this.readyState == asyncCalls.READY && this.status == asyncCalls.OK){
    var definitions = {};
    if(this.responseText){
      definitions = JSON.parse(this.responseText);
      for(value in definitions.definition){
        var paragraph = document.createElement('p');
        var definition = document.createTextNode((Number(value) + 1) + ': '+ definitions.definition[value] + '.');
        paragraph.appendChild(definition);
        definition_box.appendChild(paragraph);
        if (definitions.definition[value] != 'Word Not Found'){
          script_js = document.createElement("script");
          script_js.type = "text/javascript";
          script_js.src = "//platform.twitter.com/widgets.js";
          var twitter = document.createElement('a');
          twitter.setAttribute('href', 'https://twitter.com/share');
          twitter.setAttribute('class', 'twitter-share-button');
          twitter.setAttribute('data-url','shakespeare_search.appspot.com');
          twitter.setAttribute('data-text', asyncCalls.word.toUpperCase() + ' definition: ' + definitions.definition[value]);
          twitter.setAttribute('data-via', 'shakespeare_search');
          twitter.innerHTML = "Tweet";
          var br = document.createElement('br');
          definition_box.appendChild(script_js);
          definition_box.appendChild(twitter);
          definition_box.appendChild(br);
        }
      }
    }
  }
  else if(this.readyState == asyncCalls.READY && this.status == asyncCalls.ERROR){
    asyncCalls.sendAjax('/define?word=', asyncCalls.word, asyncCalls.definitionResponseHandler);
  }
};




asyncCalls.removeChildsFromDiv = function(div){
  while (div.hasChildNodes()) {
      div.removeChild(div.firstChild);
    }
}


asyncCalls.main = function(){
  document.getElementById('search_button').addEventListener('click', asyncCalls.setAjax, false);
  document.getElementById('search_input').addEventListener('keypress',
    function (e) {
      if (e.keyCode == asyncCalls.ENTER) {
        asyncCalls.setAjax();
      }
    }
    , false);
};





asyncCalls.main();

