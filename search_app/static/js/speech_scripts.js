/*
speech recognition extention.
Uses speech API
https://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html
*/

var speech = {}
speech.DEFAULT_LANGUAGE = 6;
speech.DEFAULT_DIALECT = 6;
speech.DEFAULT_TIME = 100;
speech.DEFAULT_MIC_HEIGHT = 110;
speech.mic_img_start = 'static/images/mic.gif';
speech.mic_img_recording = 'static/images/mic-animate.gif';
speech.mic_img_denied = 'static/images/mic-slash.gif';
speech.GO_CHROME_MESSAGE = 'install Chrome Beta and enjoy the speech experience!';
speech.WELCOME_MESSAGE = 'Hello World! Welcome to Shakespeare app.';


speech.langs =
[['Afrikaans',       ['af-ZA']],
 ['Bahasa Indonesia',['id-ID']],
 ['Bahasa Melayu',   ['ms-MY']],
 ['Català',          ['ca-ES']],
 ['Čeština',         ['cs-CZ']],
 ['Deutsch',         ['de-DE']],
 ['English',         ['en-AU', 'Australia'],
                     ['en-CA', 'Canada'],
                     ['en-IN', 'India'],
                     ['en-NZ', 'New Zealand'],
                     ['en-ZA', 'South Africa'],
                     ['en-GB', 'United Kingdom'],
                     ['en-US', 'United States']],
 ['Español',         ['es-AR', 'Argentina'],
                     ['es-BO', 'Bolivia'],
                     ['es-CL', 'Chile'],
                     ['es-CO', 'Colombia'],
                     ['es-CR', 'Costa Rica'],
                     ['es-EC', 'Ecuador'],
                     ['es-SV', 'El Salvador'],
                     ['es-ES', 'España'],
                     ['es-US', 'Estados Unidos'],
                     ['es-GT', 'Guatemala'],
                     ['es-HN', 'Honduras'],
                     ['es-MX', 'México'],
                     ['es-NI', 'Nicaragua'],
                     ['es-PA', 'Panamá'],
                     ['es-PY', 'Paraguay'],
                     ['es-PE', 'Perú'],
                     ['es-PR', 'Puerto Rico'],
                     ['es-DO', 'República Dominicana'],
                     ['es-UY', 'Uruguay'],
                     ['es-VE', 'Venezuela']],
 ['Euskara',         ['eu-ES']],
 ['Français',        ['fr-FR']],
 ['Galego',          ['gl-ES']],
 ['Hrvatski',        ['hr_HR']],
 ['IsiZulu',         ['zu-ZA']],
 ['Íslenska',        ['is-IS']],
 ['Italiano',        ['it-IT', 'Italia'],
                     ['it-CH', 'Svizzera']],
 ['Magyar',          ['hu-HU']],
 ['Nederlands',      ['nl-NL']],
 ['Norsk bokmål',    ['nb-NO']],
 ['Polski',          ['pl-PL']],
 ['Português',       ['pt-BR', 'Brasil'],
                     ['pt-PT', 'Portugal']],
 ['Română',          ['ro-RO']],
 ['Slovenčina',      ['sk-SK']],
 ['Suomi',           ['fi-FI']],
 ['Svenska',         ['sv-SE']],
 ['Türkçe',          ['tr-TR']],
 ['български',       ['bg-BG']],
 ['Pусский',         ['ru-RU']],
 ['Српски',          ['sr-RS']],
 ['한국어',            ['ko-KR']],
 ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
                     ['cmn-Hans-HK', '普通话 (香港)'],
                     ['cmn-Hant-TW', '中文 (台灣)'],
                     ['yue-Hant-HK', '粵語 (香港)']],
 ['日本語',           ['ja-JP']],
 ['Lingua latīna',   ['la']]];


speech.initButton = function(){
  /* creates the start_button with default settings
  */

  var start_button = document.createElement('Button');
  start_button.id = 'start_button';
  var mic_icon = document.createElement('img');
  mic_icon.id = 'start_img';
  mic_icon.src = speech.mic_img_start;
  mic_icon.alt = 'Start';
  mic_icon.height = speech.DEFAULT_MIC_HEIGHT;
  start_button.appendChild(mic_icon);
  start_button.addEventListener('click', speech.startButton, false);
  div_start.appendChild(start_button);

};
speech.initLangSelector = function(){
  /* creates the language selector with default values
  */

  var lang_selector = document.createElement('select');
  lang_selector.id = 'select_language';
  lang_selector.addEventListener('change', speech.updateCountry, false);


  var dialect_selector = document.createElement('select');
  dialect_selector.id = 'select_dialect';

  div_language.appendChild(lang_selector);
  div_language.appendChild(dialect_selector);

  for (var i = 0; i < speech.langs.length; i++) {
    select_language.options[i] = new Option(speech.langs[i][0], i);
  }

  select_language.selectedIndex = speech.DEFAULT_LANGUAGE;
  speech.updateCountry();
  select_dialect.selectedIndex = speech.DEFAULT_DIALECT;

};


speech.updateCountry = function() {
  /* Changes the drop down selection list of countries dialects when a new
  language is selected.
  */

  for (var i = select_dialect.options.length - 1; i >= 0; i--) {
    select_dialect.remove(i);
  }
  var list = speech.langs[select_language.selectedIndex];
  for (var i = 1; i < list.length; i++) {
    select_dialect.options.add(new Option(list[i][1], list[i][0]));
  }
  select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
};


speech.run = function() {
  /* sets the behavior of speech recognition for each event.
  */

  speech.word = '';
  speech.recognizing = false;
  speech.ignore_onend;
  speech.start_timestamp;
  start_button.style.display = 'inline-block';
  speech.recognition.continuous = false; //just one word

  speech.recognition.onstart = function() {
    /* when recognition starts, changes the mic image
    */

    speech.recognizing = true;
    start_img.src = speech.mic_img_recording;
  };

  speech.recognition.onerror = function(event) {
    /* when recognition error, discards the listening
    */

    if (event.error == 'no-speech' || event.error == 'audio-capture') {
      start_img.src = speech.mic_img_start;
      speech.ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      if (event.timeStamp - speech.start_timestamp < speech.DEFAULT_TIME) {
      }
      speech.ignore_onend = true;
    }
  };

  speech.recognition.onend = function() {
    /* When recognition ends, changes mic image
    */

    speech.recognizing = false;
    if (speech.ignore_onend) {
      return;
    }
    start_img.src = speech.mic_img_start;
    if (!speech.word) {
      return;
    }
  };

  speech.recognition.onresult = function(event) {
    /* when recognition success, retrieves word
    */

    if (typeof(event.results) == 'undefined') {
      speech.recognition.onend = null;
      speech.recognition.stop();
      return;
    }
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        speech.word += event.results[i][0].transcript;
      }
    }
    if (speech.word && speech.word != '') {
      speech.recognition.stop();
      speech.done();
    }
  };
};


speech.startButton = function(event) {
  /* Mic Button event handler to start or stop speech recognition.
  */

  if (speech.recognizing) {
    speech.recognition.stop();
    return;
  }
  speech.word = '';
  speech.recognition.lang = select_dialect.value;
  speech.recognition.start();
  speech.ignore_onend = false;
  start_img.src = speech.mic_img_denied;
  speech.start_timestamp = event.timeStamp;
};


speech.done = function(){
  /* writes the word on search input and calls to search.
  */

  document.getElementById('search_input').value = speech.word;
  if (asyncCalls){
    asyncCalls.setAjax();
  }
};

speech.get_supported_speech = function(){
  if (typeof speechRecognition !== 'undefined'){
    return new speechRecognition();
  }else if (typeof msSpeechRecognition !== 'undefined'){
    return new msSpeechRecognition();
  } else if (typeof mozSpeechRecognition !== 'undefined'){
    return new mozSpeechRecognition();
  } else if (typeof webkitSpeechRecognition !== 'undefined'){
    return new webkitSpeechRecognition();
  }else
    return false;
};


speech.say = function(sentence){
  /* Makes the app say the sentence.
  */

  if ('SpeechSynthesisUtterance' in window){
    var text = new SpeechSynthesisUtterance(sentence);
    //TODO: ?? say the word in the selected language??
    //careful! "word not found"
    speechSynthesis.speak(text);
  }
  else{
    console.log(sentence);
  }
};


speech.main = function(){
  /* main function!
  */

  speech.say(speech.WELCOME_MESSAGE);
  speech.recognition = speech.get_supported_speech();
  console.log(speech.recognition);
  if (speech.recognition){
    speech.initButton();
    speech.initLangSelector();
    speech.run();
  }else{
    speech.say(speech.GO_CHROME_MESSAGE);
  }
};


speech.main();